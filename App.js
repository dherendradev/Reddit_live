import React from 'react';
import axios from 'axios';
import './App.css';

class App extends React.Component {
      state = {
        posts : []
      };

      componentDidMount(){
        axios.get(`https://www.reddit.com/r/reactjs.json`).then(res=>{
          const posts = res.data.data.children.map(object=> object.data);
          this.setState({posts});
          console.log(posts);
        })
      }
      render() { 
        return (
            <div> 
              <h1>Posts from Reddit </h1> 
              <ul>{this.state.posts.map(post => { 
                return <li key={post.id}>{post.title} , {post.subreddit_subscribers}</li>
                  })}
              </ul>
            </div>
            ); 
          }
    }

export default App;
